//-------------------------------
// This file should be included in the content iframe or in the page (if not using a content iframe).
// this is used to call functionality that is in the parent shell (or on the current page if a content iframe is not used)
//-------------------------------

var tgContentInterface = (function() {

	function getShellInterface(){
		try{
			if(typeof window.tgShellInterface === 'undefined') {
				// this must be a content frame inside an iframe shell instead 
				// of a single page
			}
			return parent.tgShellInterface;
		}
		catch(ex) {
			window.console.error("ERROR: Can't get to the tgShellInterface.  Make sure you are running from a WebServer rather than from a file.");
			throw ex;
		}
	}

	return {
		displayError: function(title, message, autoCloseDuration, options){
			getShellInterface().displayError(title, message, autoCloseDuration, options);
		},
		displayWarning: function(title, message, autoCloseDuration, options){
			getShellInterface().displayWarning(title, message, autoCloseDuration, options);
		},
		displayInfo: function(title, message, autoCloseDuration, options){
			getShellInterface().displayInfo(title, message, autoCloseDuration, options);
		},
		displaySuccess: function(title, message, autoCloseDuration, options){
			getShellInterface().displaySuccess(title, message, autoCloseDuration, options);
		},
		displayMessage: function(title, message, autoCloseDuration, options){
			getShellInterface().displayMessage(title, message, autoCloseDuration, options);
		}
	};
})();
window.tgContentInterface = tgContentInterface;