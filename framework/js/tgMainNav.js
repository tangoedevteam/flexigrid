/*! tgMainNav - Main Navigation Widget - v1.0.0 - 2013-10-17
* https://github.com/tgui/tgMainNav
* Copyright (c) 2013 Tangoe */
(function($) {
	$.widget("tg.tgMainNav", {
		options: {
			jsonModel: undefined,
			defaultMainOptionId: undefined,
			timeoutInterval: 100,
			clickHandlerFunction: undefined,
			breadcrumbsHandlerFunction: undefined,
			suboptionContentFunction: undefined
		},

		_create : function() {
			var self = this;
			self._element = $(self.element); 
			self._bActiveS2 = false;
			self._bActiveS1 = false;
			self._bActiveM = false;
			self._timerMain = 0;
			self._timerS1 = 0;
			self._timerS2 = 0;
			self._lastMainId = "";
			self._lastMain = "";
			self._lastS1 = "";
			self._lastS2 = "";
			self._jasonModelObj = self.options.jsonModel;
			self._s1PrevLinkId = [];
			self._activeLinkId = self.options.defaultMainOptionId;

			$(self.element).addClass("tgCOMP-mainNav ui-widget");

			if (self.options.defaultMainOptionId !== undefined) {
				self.setActiveMainNavOption(self.options.defaultMainOptionId);
			}

			var _subnv1 = self._createSubnavContainerHTML('tg-subNav1');
			var _subnv2 = self._createSubnavContainerHTML('tg-subNav2');

			var tgMenu = self.element;
			tgMenu.prepend(_subnv1);
			tgMenu.prepend(_subnv2);

			$("#tg-subNav1", self.element).addClass("subNavOff");
			$("#tg-subNav2", self.element).addClass("subNavOff");

			$("#tg-subNav1")
				.on("mouseenter", function() {
					self._bActiveS1 = true;
					clearTimeout(self._timerS1);
				})
				.on("mouseleave", function() {
					self._bActiveS1 = false;
					self._timerS1 = setTimeout(function() {
						if (!self._bActiveM) {
							if (!self.bActiveS2) {
								if ($("#tg-subNav1").hasClass("subNavOn")) {
									self._destroySubNavPanel('tg-subNav1');
									self._deselectMainLinks();
									if ($("#tg-subNav2").hasClass("subNavOn")) {
										self._destroySubNavPanel('tg-subNav2');
									}
								}
								self._s1PrevLinkId = [];
							}
						}},
						self.options.timeoutInterval
					);
				});

			$("#tg-subNav2")
				.on("mouseenter", function() {
					self._bActiveS2 = true;
					clearTimeout(self._timerS1);
				})
				.on("mouseleave",	function() {
					self._bActiveS2 = false;
					self._destroySubNavPanel('tg-subNav2');
					if (self._s1PrevLinkId.length > 0) {
						$.each(self._s1PrevLinkId, function(index, value) {
							var lnkSlctr = "#"+ value;
							if ($(lnkSlctr).hasClass("hasChildren")) {
								$(lnkSlctr).removeClass("optionOn");
								$(lnkSlctr).addClass("optionOff");
							}
						});
					}
					self._timerS1 = setTimeout(function() {
							if (!self.bActiveS2) {
								if ($("#tg-subNav1").hasClass("subNavOn")) {
									self._destroySubNavPanel('tg-subNav1');
									self._deselectMainLinks();
								}
							}
						},
						self.options.timeoutInterval
					);
				});

			// bind main nav options
			$("ul li", this._element)
				.addClass("subNavOff")
				.mouseenter(function() {
					var imgSrcOn = $("a[data-image-on]", this).data("image-on");
					$("a[data-image-on]", this).children("img").attr("src", imgSrcOn);
				})
				.mouseleave(function() {
					self._bActiveM = false;
					if (!$(this).hasClass("current")) {
						var imgSrc = $("a[data-image]", this).data("image");
						$("a[data-image]", this).children("img").attr("src", imgSrc);
					}
					self._timerMain = setTimeout(function() {
						var lnkSlctr = "#" + self._lastMainId;
						if (self._bActiveM || (self._bActiveS1 || self._bActiveS2)) {
							$(lnkSlctr).parent().addClass('linkOn');
						} else {
							self._destroyAllSubNavPanels();
						}
					},
					self.options.timeoutInterval);
				});
			$("ul li a", this._element)
				.mouseenter( function(){self._lastMain = this.innerText; self._lastMainId = this.id; self._bActiveM = true;} )
				//.mouseenter( function(){ $(this).parent().removeClass("linkOff"); $(this).parent().addClass("linkOn");})
				.mouseenter( function(){ 
					$(".linkOn", $(this).closest('ul')).removeClass("linkOn");
					$(this).parent().addClass("linkOn");
				})
				.mouseenter({self: this}, self._showSubNav1)
				.click(function() {
					self._clickMainNavOption(this.id);
					return false;
				})
				.mouseenter(function() {
					self._lastMain = this.innerText;
					self._lastMainId = this.id;
					self._bActiveM = true;
					$(this).parent().addClass("linkOn");
				})
				.mouseleave(function() {
					self._bActiveM = false;
					self._timerMain = setTimeout(function() {
						var lnkSlctr = "#" + self._lastMainId;
						if (self._bActiveM || (self._bActiveS1 || self._bActiveS2)) {
							$(lnkSlctr).parent().addClass('linkOn');
						} else {
							self._destroyAllSubNavPanels();
						}
					},
					self.options.timeoutInterval);
				});
			$("ul li a[data-image-on]", this._element).each(function(index, node) {
					if ($(node).parent().hasClass("current")) {
						var srcImageOn = $("ul li a[data-image-on]", this._element).data("image-on");
						$(node).children("img").attr("src", srcImageOn);
					}
				});
		},

		destroy : function() {
			var self = this;
			self._bActiveS2 = false;
			self._bActiveS1 = false;
			self._bActiveM = false;
			self._timerMain = 0;
			self._timerS1 = 0;
			self._timerS2 = 0;
			self._lastMainId = "";
			self._lastMain = "";
			self._lastS1 = "";
			self._lastS2 = "";
			self._jasonModelObj = undefined;
			self._s1PrevLinkId = [];
			self._activeLinkId = undefined;
			$(self.element).removeClass("tgCOMP-mainNav ui-widget");
			$.Widget.prototype.destroy.apply(self, arguments);
		},

        _createSubnavContainerHTML: function(elementId){
            var _container = "<div id='"+ elementId +"'>" +
                             "<table cellspacing='0' cellpadding='0' border='0'>" +
                             "<tbody id='" + elementId + "TB'></tbody>" +
                             "</table></div>";

            return _container;
        },

		_buildLinkDataObject : function(linkId, node) {
			var oLink = {};
			oLink.id = linkId;
			for ( var prpty in node) {
				if (typeof node.childOptions !== "undefined" && 
					prpty !== "childOptions") {
					oLink[prpty] = node[prpty];
				}
			}
			return oLink;
		},

		_showSubNav1 : function(event) {
			if (event.data.self._bActiveM) {
				if ($("#tg-subNav1").hasClass("subNavOn")) {
					event.data.self._destroyAllSubNavPanels();
				}
			}

			if ($("#tg-subNav1").hasClass("subNavOff")) {
				var srcLeft = $(this).parent().position().left + $(this).parent().parent().position().left;
				var srcTop =  $(this).offsetParent().position().top + $(this).offsetParent().height() - 1;
				event.data.self.showSubNav1(this.id, srcTop, srcLeft);
			}
		},

		showSubNav1 : function(jsonCurrNodeId, srcTop, srcLeft) {
			// store the selected mainNav option Id for use by
			// the level 2 panel

			var popupPanel = $("#tg-subNav1", this.element);
			popupPanel.data('mainNavOptionId', jsonCurrNodeId);

			var self = this;
			var jsonModelObj = self.options.jsonModel;
			var jsonCurrNode = jsonModelObj[jsonCurrNodeId].childOptions;
			var labelContent = null;
			var tdOption = null;

			if (typeof jsonCurrNode !== "undefined" && jsonCurrNode !== "") {
				$.each(jsonCurrNode, function(index, node) {
					// build link data and store
					// it in the subnav panel
					var objLink = self._buildLinkDataObject(index, node);
					$("#tg-subNav1TB").data(index,	objLink);

					// when configured, override
					// label with custom content
					// generation function
					labelContent = node.label;
					if (self.options.suboptionContentFunction !== undefined) {
						labelContent = window[self.options.suboptionContentFunction](node.label);
					}

					// build up link row and
					// append to panel container
					tdOption = "";
					tdOption += "<tr><td id='"+index+"' class='tgCOMP-menu-item ";

					if ($(node.childOptions).length > 0) {
						tdOption += "hasChildren optionOff";
					}
					else {
						tdOption += "noChildren optionOff";
					}

					tdOption += "'>" + labelContent + "</td></tr>";
					
					var $newRow = $(tdOption).appendTo("#tg-subNav1TB");

					// bind events
					if ($(node.childOptions).length > 0) {
						$newRow
							.mouseenter(function(evt) {
								if (!evt) {
									evt = window.event;
								}
								var evtSrc = evt.target || evt.srcElement;
								$(evtSrc).addClass("hasChildren");
								$(evtSrc).removeClass("optionOff");
								$(evtSrc).addClass("optionOn");

								self._lastS1 = this.innerText;
								self._destroySubNavPanel('tg-subNav2');
								// build the level 2  panel
								self._showSubNav2(evt, evtSrc.id);
							})
							.mouseleave(function(evt) {
								if (!evt) {
									evt = window.event;
								}
								var evtSrc = evt.target || evt.srcElement;
								self._s1PrevLinkId.push(evtSrc.id);
								clearTimeout(self._timerS1);

								$(evtSrc).removeClass("optionOn");
								$(evtSrc).addClass("optionOff");

								var lastS1PrevId = self._s1PrevLinkId.slice(-1);
								self._timerS1 = setTimeout(function() {
									if (self._bActiveS2) {
										var lnkSlctr = "#" + lastS1PrevId;
										$(lnkSlctr).removeClass("optionOff");
										$(lnkSlctr).addClass('optionOn');
									}
								}, self.options.timeoutInterval);
							});
					} else {
						$newRow
							.click({self: self, optionData: node, level: "s1"}, self._clickSubNavOption)
							.click(function(){
								var imgSrc = $("ul li a[data-image]", self._element).data("image");
								$("ul li a[data-image]", self._element).children("img").attr("src", imgSrc);
							})
							.mouseenter(function(evt) {
								if (!evt) {
									evt = window.event;
								}
								var evtSrc = evt.target || evt.srcElement;
								$(evtSrc).addClass("noChildren");
								$(evtSrc).removeClass("optionOff");
								$(evtSrc).addClass("optionOn");
								self._lastS1 = this.innerText;
								self._destroySubNavPanel('tg-subNav2');
							})
							.mouseleave(function(evt) {
								if (!evt) {
									evt = window.event;
								}
								var evtSrc = evt.target || evt.srcElement;
								$(evtSrc).removeClass("optionOn");
								$(evtSrc).addClass("optionOff");
							});
					}
				});

				// position and display subnav panel
				var $objPanel = $("#tg-subNav1");

				self._realignPanelIfCutoff($objPanel, srcTop, srcLeft, 10);
			}
		},

		_realignPanelIfCutoff : function($panel, proposedTop, proposedLeft, zindex) {
			var panelId = $panel.attr("id");

			// calc viewport dimensions
			var winRight = $(window).scrollLeft() + $(window).width();
			var winBottom = $(window).scrollTop() + $(window).height();

			// calc panel dimensions
			//var $pnl = $panel.offset();
			var $pWidth = $panel.outerWidth();
			var pBottom = proposedTop + $panel.outerHeight();
			var pRight = proposedLeft + $pWidth;

			if (panelId === "tg-subNav2") {
				$panel.height("auto");
				if ($panel.outerHeight() < $("#tg-subNav1").outerHeight()) {
					$panel.height($("#tg-subNav1").height());
				}
			}
			
			// test if panel extends below viewport
			var rtOver = winRight - pRight -  $panel.parent().offset().left; // also subtract the left position of the menu itself from the left side of the page
			var btmOver = winBottom - pBottom;
			var rtIsCutoff = (rtOver < 0) ? true : false;
			var btmIsCutoff = (btmOver < 0) ? true : false;

			// realign panel when cutoff  
			if (rtIsCutoff) {
				if (panelId === "tg-subNav2") {
					var $panelS1 = $("#tg-subNav1");
					proposedLeft = ($panelS1.position().left - $pWidth) + "px";
				} else {
					proposedLeft = (proposedLeft + rtOver) + "px";
				}
			}

			if (btmIsCutoff) {
				proposedTop = proposedTop + btmOver;
			}

			$panel.removeClass("subNavOff");
			$panel.addClass("subNavOn");
			$panel.css("top", proposedTop);
			$panel.css("left", proposedLeft);
			$panel.css("z-index", zindex);
		},

		_showSubNav2 : function(e, jsonCurrNodeId) {
			// get the stored ID for the selected mainNav option
			var popupPanel = $('#tg-subNav1');
			var jsonParentNodeId = popupPanel.data('mainNavOptionId');

			if (!e) {
				e = window.event;
			}
			//var evtSrc = e.target || e.srcElement;

			var self = this;
			var srcLeft = $("#tg-subNav1").position().left + $("#tg-subNav1").width();
			var srcTop = $("#tg-subNav1").position().top;
			var jsonModelObj = self.options.jsonModel;

			var jsonCurrNode = jsonModelObj[jsonParentNodeId].childOptions[jsonCurrNodeId].childOptions;

			if (typeof jsonCurrNode !== "undefined" && jsonCurrNode !== "") {
				var labelContent = null;
				var tdOption = null;
				$.each(jsonCurrNode, function(index, node) {
					// build link data and store
					// it in the subnav panel
					var objLink = self._buildLinkDataObject(index, node);
					jQuery("#tg-subNav2TB").data(index, objLink);

					// when configured, override
					// label with custom content
					// generation function
					labelContent = node.label;
					if (self.options.suboptionContentFunction !== undefined) {
						labelContent = window[self.options.suboptionContentFunction](node.label);
					}

					// build up link row and
					// append to panel container
					tdOption = "";
					tdOption += "<tr><td id='"+index+"' class='tgCOMP-menu-item noChildren optionOff'>" + labelContent + "</td></tr>";

					var $newRow = jQuery(tdOption).appendTo("#tg-subNav2TB");

					// bind events
					$newRow
						.click({self: self, optionData: node, level: "s2"}, self._clickSubNavOption)
						.click(function(){
							var imgSrc = $("ul li a[data-image]", self._element).data("image");
							$("ul li a[data-image]", self._element).children("img").attr("src", imgSrc);
						})
						.mouseenter(function(evt) {
							if (!evt) {
								evt = window.event;
							}
							var evtSrc = evt.target || evt.srcElement;
							$(evtSrc).addClass("noChildren");
							$(evtSrc).removeClass("optionOff");
							$(evtSrc).addClass("optionOn");
							self._lastS2 = this.innerText;
						})
						.mouseleave(function(evt) {
							if (!evt) {
								evt = window.event;
							}
							var evtSrc = evt.target || evt.srcElement;
							$(evtSrc).removeClass("optionOn");
							$(evtSrc).addClass("optionOff");
						});
				});

				// position and display subnav panel
				var $objPanel = $("#tg-subNav2");

				self._realignPanelIfCutoff($objPanel, srcTop, srcLeft, 11);
			}
		},

		_cleanUpMain : function() {
			if (!this._bActiveS1) {
				if ($("#tg-subNav1").hasClass("tg-subNavOn")) {
					this._destroySubNavPanel('tg-subNav1');
				}
				this._deselectMainLinks();
			}
		},

		_deselectMainLinks : function() {
			$("ul li", this._element).removeClass("linkOn");
			this._bActiveM = false;
		},

		_cleanUpSubNav : function() {
			if (!this.bActiveS2) {
				if ($("#tg-subNav1").hasClass("subNavOn")) {
					this._destroySubNavPanel('tg-subNav1');
				}
				this._deselectMainLinks();
			}
		},

		_destroySubNavPanel : function(panelId) {
			var pnlSlctr = "#" + panelId;
			var pnlTBody = pnlSlctr + "TB";
			var bIsOpen = $(pnlSlctr).hasClass("subNavOn");

			if (bIsOpen) {
				$(pnlTBody).html("");
			}

			if (panelId === "tg-subNav1") {
				var popupPanel = $("#" + panelId);
				jQuery(popupPanel).data('mainNavOptionId', '');
			}

			$(pnlSlctr).removeClass("subNavOn").addClass("subNavOff");
			$(pnlSlctr).css("left", 0).css("top", 0);
		},

		_destroyAllSubNavPanels : function() {
			var self = this;
			$("#tg-subNav1, #tg-subNav2").each(function() {
				self._destroySubNavPanel(this.id);
			});
			self._bActiveS1 = false;
			self._bActiveS2 = false;
			self._deselectMainLinks();
		},

		_clickMainNavOption : function(linkId) {
			var self = this;
			var jsonModelObj = self.options.jsonModel;
			var jsnData = jsonModelObj[linkId];
			if (typeof jsnData.childOptions === "object") {
				return false;
			}

			self.setActiveMainNavOption(linkId);
			self._setBreadcrumbs("main");
			self._destroyAllSubNavPanels();
			self._tgMenuClicked(jsnData, linkId);
		},

		_tgMenuClicked : function(jsnData, linkId) {
			var self = this;
			var menuClickFunction = self.options.clickHandlerFunction;
			self._activeLinkId = linkId;
			if (menuClickFunction !== "") {
				window[menuClickFunction](jsnData);
			}
		},

		_clickSubNavOption : function(event) {
			event.data.self.setActiveMainNavOption(event.data.self._lastMainId);
			event.data.self._setBreadcrumbs(event.data.level, event.data.optionData);
			event.data.self._destroyAllSubNavPanels();
			event.data.self._tgMenuClicked(event.data.optionData, event.data.optionData.id);
		},

		setActiveMainNavOption : function(mainNavLinkId) {
			var navSlctr = "#tngoMainNav #" + mainNavLinkId;
			var $navOpt = $(navSlctr).parent();
			if (!$navOpt.hasClass("current")) {
				$("#tngoMainNav ul li").removeClass("current");
				$navOpt.addClass("current");
			}
		},

		_setBreadcrumbs : function(optionLevelClicked,
				optionData) {
			var self = this;
			var crumbFunction = self.options.breadcrumbsHandlerFunction;
			if (crumbFunction !== "") {
				window[crumbFunction](optionLevelClicked, self._lastMain, self._lastS1, self._lastS2, optionData);
				self._lastMain = "";
				self._lastS1 = "";
				self._lastS2 = "";
			}
		},

		getActiveLinkId : function() {
			return this._activeLinkId;
		},

		setBreadcrumbsForAction : function(moduleMenuId, actionValue) {
			if (moduleMenuId !== undefined && actionValue !== undefined) {
				var optionLevelClicked = "main";
				var labelMain = "";
				var labelS1 = "";
				var altlabelS1 = "";
				var labelS2 = "";
				//var optionData = undefined;
				var self = this;
				var jsonNodesMain = self.options.jsonModel[moduleMenuId];
				var jsonNodesS1 = null;
				var jsonNodesS2 = null;
				var optionData = null;

				labelMain = jsonNodesMain.label;
				jsonNodesS1 = jsonNodesMain.childOptions;

				if (jsonNodesMain.action !== undefined && jsonNodesMain.action === actionValue) {
					optionData = jsonNodesMain;
				} else if (typeof jsonNodesS1 !== "undefined" && jsonNodesS1 !== "") {
					$.each(jsonNodesS1, function(index, node) {
						if (typeof node.action !== "undefined" && node.action === actionValue) {
							optionLevelClicked = "s1";
							labelS1 = node.label;
							optionData = node;
						} else if (typeof node.childOptions !== "undefined") {
							altlabelS1 = node.label;
							jsonNodesS2 = node.childOptions;
							$.each(jsonNodesS2, function(index2, node2) {
								if (typeof node2.action !== "undefined" && node2.action === actionValue) {
									optionLevelClicked = "s2";
									labelS1 = altlabelS1;
									labelS2 = node2.label;
									optionData = node2;
								}
							});
						}
					});
				} else {
					return false;
				}

				var crumbFunction = self.options.breadcrumbsHandlerFunction;
				if (crumbFunction !== "") {
					window[crumbFunction](optionLevelClicked, labelMain, labelS1, labelS2, optionData);
					self.setActiveMainNavOption(moduleMenuId);
					self._lastMain = "";
					self._lastS1 = "";
					self._lastS2 = "";
					return true;
				}
			}
			return false;
		}
	});
})(jQuery);