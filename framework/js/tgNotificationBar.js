/*
 * tgJS
 *
 * Copyright (c) 2013 Tangoe
 */
(function($) {
    if (typeof $.tg !== 'undefined' && typeof $.tg.tgNotificationBar !== 'undefined') {
        return; //already defined so get out
    }

    $.widget("tg.tgNotificationBar", {
        options: {
            standard: {
                fadeInDuration: 400,
                fadeOutDuration: 800,
                autoCloseDuration: 5000
            },
            error: { autoCloseDuration: 25000 },
            warning: { autoCloseDuration: 25000 },
            info: { autoCloseDuration: 10000 },
            message: { autoCloseDuration: 10000 },
            success: { autoCloseDuration: 5000 }
        },
        
        _element: undefined,
        _timer: undefined,
        _timeShown: undefined,
        _minDuration: undefined,
        _sharedData: {},
        _document: undefined,
        _unbind: null,
        _hoveringOverNotification: false,
        _hideOnHoverOut: false,

        _create : function() {
            // this should be called on an empty div

            var self = this;
            var $self = $(this);
            this._document = $(document);
            this._element = self._element = $(self.element); 

            // create the nessary markup

            // add a 'tg-notifyBar' class to the element (div) that we are connected to
            this._element.addClass('tg-notifybar tg-clearfix');

            // add the child elements needed to support the notification bar
            this._element.append("<div class='tg-notifybar__header'><div class='tg-notifybar__header__title'></div><div class='tg-notifybar__header__close'>X<div></div>")
                .append("<div class='tg-notifybar__body'></div>")
                .append("<div class='tg-notifybar__icon'></div>")
                .append("<div class='tg-notifybar__sizer'>...</div>");

            // hook up the close button
            $(".tg-notifybar__header__close", this.element).click(function () {
                self.clear();
            });

            // hookup mouse capture code
            this._element.on('mousedown', {notifyBarWidget: self}, function(e) {
                if(typeof self._unbind !== undefined &&  self._unbind !== null){
                    self._unbind();
                    self._unbind = null;
                }

                self._sharedData.lastY = e.screenY;

                $("<div id='____notifify_shim____' style='z-index: 500; position: absolute; top: 0; left: 0; bottom: 0; right: 0; background-color: transparent;'></div>").appendTo('body', this._document);

                self._document.on('mousemove', {notifyBarWidget: self}, self._mouseOver);
                
                self._unbind = function() {
                    self._document.unbind("mousemove", self._mouseOver);
                    $('#____notifify_shim____').remove();
                };
                
                var upHandler = function(/*e*/) {
                    if(typeof self._unbind !== undefined &&  self._unbind !== null){
                        self._unbind();
                        self._unbind = null;
                    }
                };
                
                self._document.on('mouseup', upHandler);
                
                return self._mouseDown($self, e);
            });

            // hookup hover to be able to not timeout while the user is hovering over the notification
            this._element.hover(
                function(/*e*/) {
                    // hover in
                    self._hoveringOverNotification = true;
                },
                function(/*e*/) {
                    // hover out
                    self._hoveringOverNotification = false;
                    if(self._hideOnHoverOut){
                        self.clear();
                    }
                    self._hideOnHoverOut = false;
                }
            );
        },

        _init: function () {
        },

        _setOption: function (/*key , value*/) {
            $.Widget.prototype._setOption.apply(this, arguments);
        },

        autoClear: function () {
            if (this._timeShown == null || (this._minDuration <= 0) ||
                (new Date() - this._timeShown) > this._minDuration) {
                this._clear(null, this);
            }
        },

        clear: function (fadeOutDuration) {
            this._clear(null, this, fadeOutDuration);
        },

        _clear: function (callback, self, fadeOutDuration) {
            if (fadeOutDuration < 0) {
                fadeOutDuration = self._getFadeOutDuration(self._currentType);
            }
            self._element.fadeOut(fadeOutDuration,
            function () {
                self._hideOnHoverOut = false;
                $(".tg-notifybar__header__title", self.element).text("");
                $(".tg-notifybar__body", self.element).text("").css("height", "");
                if(typeof self._unbind !== undefined &&  self._unbind !== null){
                    self._unbind();
                    self._unbind = null;
                }
                if (self._currentType) {
                    self._element.removeClass("tg-notifybar--" + self._currentType);
                    self._currentType = undefined;
                }
                if (callback) {
                    callback();
                }
            });
        },

        displayMessage: function (type, title, msg, autoCloseDuration, options) {
            var displayOptions = options || {};
            displayOptions.autoCloseDuration = autoCloseDuration;
            if (!displayOptions.autoCloseDuration) {
                displayOptions.autoCloseDuration = this._getAutoCloseDuration(type);
            }

            if (displayOptions.minDuration && displayOptions.autoCloseDuration > 0 && displayOptions.minDuration > displayOptions.autoCloseDuration) {
                displayOptions.autoCloseDuration = displayOptions.minDuration;
            }

            if (!displayOptions.fadeInDuration) {
                displayOptions.fadeInDuration = this._getFadeInDuration(type);
            }

            var self = this;
            if (this._timer) {
                window.clearTimeout(this._timer);
            }
            this._clear(function () {
                self._element.addClass("tg-notifybar--" + type);
                self._currentType = type;
                self._timeShown = new Date();
                self._minDuration = displayOptions.minDuration;
                $(".tg-notifybar__header__title", self.element).text(title);
                $(".tg-notifybar__body", self.element).html(msg);
                $(".tg-notifybar__icon", self.element)[0].className = $(".tg-notifybar__icon", self.element)[0].className.replace(/\btgIMG-notification--.*?\b/g, '');
                $(".tg-notifybar__icon", self.element).addClass("tgIMG-notification--" + type);
                self._element.fadeIn(displayOptions.fadeInDuration, function () {
                    var currentHeight = $(self.element).height();
                    var maxHeight = Math.floor($(window).height() * 0.75);
                    if(currentHeight > maxHeight) {
                        var newBodyHeight = maxHeight - $(".tg-notifybar__header", self.element).height() - $(".tg-notifybar__sizer", self.element).height();
                        $(".tg-notifybar__body", self.element).height(newBodyHeight);
                    }
                    if (displayOptions.autoCloseDuration && displayOptions.autoCloseDuration > 0) {
                        self._timer = window.setTimeout(function () {
                            self._timer = null;
                            if(self._hoveringOverNotification) {
                                self._hideOnHoverOut = true;
                            }
                            else {
                                self._clear(null, self);
                            }
                        }, displayOptions.autoCloseDuration);
                    }
                });
            }, self, displayOptions.fadeOutDuration);
        },

        _getFadeInDuration: function (type) {
            var fadeInDuration = this.options.standard.fadeInDuration;
            var settings = this._getSettings(type);
            if (settings.fadeInDuration) {
                fadeInDuration = settings.fadeInDuration;
            }
            return fadeInDuration;
        },

        _getFadeOutDuration: function (type) {
            var fadeOutDuration = this.options.standard.fadeOutDuration;
            var settings = this._getSettings(type);
            if (settings.fadeOutDuration) {
                fadeOutDuration = settings.fadeOutDuration;
            }
            return fadeOutDuration;
        },

        _getAutoCloseDuration: function (type) {
            var autoCloseDuration = this.options.standard.autoCloseDuration;
            var settings = this._getSettings(type);
            if (settings.autoCloseDuration) {
                autoCloseDuration = settings.autoCloseDuration;
            }
            return autoCloseDuration;
        },

        _getSettings: function (type) {
            var settings = this.options[type];
            if (!settings) {
                settings = {};
            }
            return settings;
        },

        _mouseDown: function(e){
            this._sharedData.lastY = e.screenY;
        },

        _mouseOver: function(e){
            var delta = e.screenY - e.data.notifyBarWidget._sharedData.lastY;
            e.data.notifyBarWidget._sharedData.lastY = e.screenY;
            var currentHeight = $(".tg-notifybar__body", e.data.notifyBarWidget._element).height();
            var newheight = currentHeight + delta;
            $(".tg-notifybar__body", e.data.notifyBarWidget._element).height(newheight);
        },

        destroy : function() {
            var self = this;
            // cleanup code here ...

            // remove all of the child elements
            this._element.empty();

            // remove the tg-notifier classes from the element
            this._element.removeClass('tg-notifyBar');

            
            $.Widget.prototype.destroy.apply(self, arguments);
        }
    });
})(jQuery);
