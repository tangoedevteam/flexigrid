/*
 * tgPanel and tgPanelController
 *
 * Copyright (c) 2013 Tangoe
 */

 // This widget expects to be attached to a block container element (usually a div)
 // That contains two child divs, the first child is for the heading and the second
 // is for the body of the panel
(function($) {
    if (typeof $.tg !== 'undefined' && typeof $.tg.tgPanel !== 'undefined') {
        return; //already defined so get out
    }

    $.widget("tg.tgPanel", {
        options: {
            collaspable: true,
            open: true
        },

        _element: undefined,

        _create : function() {
            var self = this;
            this._element = self._element = $(self.element); 

            // add the tg-panel class (if its not already there)
            this._element.addClass('tg-panel');
            // get the first child and make it the header
            var header = this._element.children(':first-child').addClass('tg-panel__header');
            // get the 2nd child (hopefully last) and make it the body
            this._element.children(':nth-child(2)').addClass('tg-panel__body');

            if(this.options.collaspable === true) {
                this._element
                    .removeClass(this.options.open === true ? 'tg-panel--closed' : 'tg-panel--open')
                    .addClass(this.options.open === true ? 'tg-panel--open' : 'tg-panel--closed');

                header.on('click', {panel: self}, function(event){
                    event.data.panel.toggle();
                }); 
            }
        },

        _init: function () {
        },

        _setOption: function (/*key , value*/) {
            $.Widget.prototype._setOption.apply(this, arguments);
        },

        open: function(){
            this._element.removeClass("tg-panel--closed").addClass("tg-panel--open");
        },

        close: function(){
            this._element.removeClass("tg-panel--open").addClass("tg-panel--closed");
        },

        toggle: function(){
            if(this._element.hasClass('tg-panel--open')) {
                this.close();
            }
            else {        
                this.open();
            }
        },

        destroy : function() {
            var self = this;
            // cleanup code here ...

            // remove the tg-notifier classes from the element
            this._element.removeClass('tg-panel tg-panel--open tg-panel--closed');

            // get the first child and remove the panel header class
            $(':first-child(2)', this._element).removeClass('tg-panel__header');

            // get the 2nd child (hopefully last) and remove the panel body class
            $(':nth-child(2)', this._element).removeClass('tg-panel__body');

            
            $.Widget.prototype.destroy.apply(self, arguments);
        }
    });
})(jQuery);

(function($) {
    if (typeof $.tg !== 'undefined' && typeof $.tg.tgPanelController !== 'undefined') {
        return; //already defined so get out
    }

    $.widget("tg.tgPanelController", {
        options: {
          openAllText: 'Open All',
          closeAllText: 'Close All',
          panels: null,
          parentContainerSelector: null,
          panelSelector: null, 
          showOpenAll: true
        },

        _element: undefined,
        _actionBtn: undefined,
        _panels: null,

        _create : function() {
            var self = this;
            this._element = self._element = $(self.element); 

            // add the tg-panelController class (if its not already there)
            this._element.addClass('tg-panelController');

            // find all of the associated panels
            this._panels = $(this.option('panels'));

            var selectedPanels = null;
            var parentContainer = null;
            if(this.option('parentContainerSelector') !== null && this.option('panelSelector') !== null) {
                // if both the parentContainerSelector and the panel selector are specified, then 
                // get all of the children that are contained in the parent selector
                parentContainer = $(this.option('parentContainerSelector'));
                selectedPanels = $(this.option('panelSelector'), parentContainer).filter('.tg-panel');
            }
            else if(this.option('parentContainerSelector') !== null){
                // if only the parent container selector is specified, then get all of the tgPanels that
                // are contained inside that container
                parentContainer = $(this.option('parentContainerSelector'));
                selectedPanels = $('.tg-panel', parentContainer);
            }
            else if(this.option('panelSelector') !== null){
                // if only the panel selector is specified, then get all of the 
                // panels for that selector on te page
                selectedPanels = $(this.option('panelSelector'), parentContainer).filter('.tg-panel');
            }
            if(selectedPanels !== null){
                this._panels = selectedPanels;
            }

            // TODO: ADD MARKUP HERE - BELOW IS JUST TEST CODE
            this._actionBtn = $('<a class="tg-panelController__button"></a>');
            this._element.append(this._actionBtn);
            this._initActionButton();

            // hookup the actions
            this._actionBtn.on('click', {controller: self}, function(event){
                if(event.data.controller.option('showOpenAll')) {
                    event.data.controller.openAll();
                }
                else {
                    event.data.controller.closeAll();   
                }
            });
        },

        _init: function () {
        },

        _setOption: function (/*key , value*/) {
            $.Widget.prototype._setOption.apply(this, arguments);
        },

        _initActionButton: function(){
            if(this.option('showOpenAll')) {
                this._actionBtn.text(this.option('openAllText'));
                this._element.removeClass('tg-panelController--close-all').addClass('tg-panelController--open-all');
            }
            else {
                this._actionBtn.text(this.option('closeAllText'));
                this._element.removeClass('tg-panelController--open-all').addClass('tg-panelController--close-all');
            }
        },

        openAll: function(){
            this._panels.tgPanel('open');
            this.option('showOpenAll', false);
            this._initActionButton();
        },

        closeAll: function(){
            this._panels.tgPanel('close');
            this.option('showOpenAll', true);
            this._initActionButton();
        },

        toggleAll: function(){
            this._panels.tgPanel('toggle');
        },

        destroy : function() {
            var self = this;
            // cleanup code here ...
            self._element = null;
            self._actionBtn = null;
            self._panels =  null;


            // remove the tg-notifier classes from the element
            self._element.removeClass('tg-panelController');

            // remove all of the element
            self._element.empty();
            
            $.Widget.prototype.destroy.apply(self, arguments);
        }
    });
})(jQuery);

