//-------------------------------
// This file should be included in the main shell or in the page (if not using a content iframe).
// this is used to call functionality that is in the main shell
//-------------------------------

var tgShellInterface = (function($) {

	var notifyBar = null;

	function getTitle(title, defaultTitle){
		if(typeof title !== 'string' || title.length === 0) {
			title = defaultTitle;
		}
		return title;
	}

	$(document).ready(function(){
		var notificationBarDiv = $('<div id="notificationBar" class="tg-space-half-sides tg-TABLET-flush-sides"></div>');
		$(document.body).append(notificationBarDiv);
		notifyBar = notificationBarDiv.tgNotificationBar();
	});

	return {
		displayError: function(title, message, autoCloseDuration, options){
			notifyBar.tgNotificationBar('displayMessage', 'error', getTitle(title, 'Error'), message, autoCloseDuration, options);
		},
		displayWarning: function(title, message, autoCloseDuration, options){
			notifyBar.tgNotificationBar('displayMessage', 'warning', getTitle(title, 'Warning'), message, autoCloseDuration, options);
		},
		displayInfo: function(title, message, autoCloseDuration, options){
			notifyBar.tgNotificationBar('displayMessage', 'info', getTitle(title, 'Info'), message, autoCloseDuration, options);
		},
		displaySuccess: function(title, message, autoCloseDuration, options){
			notifyBar.tgNotificationBar('displayMessage', 'success', getTitle(title, 'Success'), message, autoCloseDuration, options);
		},
		displayMessage: function(title, message, autoCloseDuration, options){
			notifyBar.tgNotificationBar('displayMessage', 'message', getTitle(title, 'Message'), message, autoCloseDuration, options);
		}
	};
})(jQuery);
window.tgShellInterface = tgShellInterface;