/*
 * tgSlideOut and tgSlideOutController
 *
 * Copyright (c) 2013 Tangoe
 */

(function($) {
    if (typeof $.tg !== 'undefined' && typeof $.tg.tgSlideOut !== 'undefined') {
        return; //already defined so get out
    }

    $.widget("tg.tgSlideOut", {
        options: {
            position: "left", // this can be "left" or "right"
            allowPin: false,  // this is used when the slide out is in a container that allows pinning
            closeBtnClass: 'tgIMG-icon--clear',
            closeBtnTooltip: 'Close',
            pinBtnClass: 'tgIMG-icon--unpin',
            pinBtnTooltip: 'Click to unpin',
            unpinBtnClass: 'tgIMG-icon--pin',
            unpinBtnTooltip: 'Click to pin',
        },

        _element: undefined,

        _create : function() {
            var self = this;
            this._element = self._element = $(self.element); 
            // add the slide out and position class to the panel
            this._element.addClass("tg-slideout").addClass("tg-slideout--" + this.options.position);

            // handle the close button
            $('.tg-slideout__close', this._element)
                .addClass(this.options.closeBtnClass)
                .attr('title', this.options.closeBtnTooltip)
                .on('click', function(){
                    if(self._element.parent().hasClass('tg-slideoutContainer')) {
                        // if the parent is a tg-slideoutContainer then tell it to close
                        self._trigger('closeContainer');
                    }
                    else {
                        self.close();
                    }
                });

            if(this.options.allowPin) {
                this.showUnpinned();
                $('.tg-slideout__pin', this._element).on('click', function(){
                    self._trigger('togglePin');
                });
            }
        },

        _init: function () {
        },

        _setOption: function (/*key , value*/) {
            $.Widget.prototype._setOption.apply(this, arguments);
        },

        open: function(){
            if(this._trigger('beforeOpen', null, [this._element.data('slideout-id')]) === false) {
                return;
            }
            if(this.isParentPinned()){
                this.showPinned();
            }
            var self = this;
            this._element.addClass("tg-slideout--transition tg-slideout--open tg-slideout-top");
            this._trigger('opened', null, [this._element.data('slideout-id')]);
            setTimeout(function(){
                self._element.removeClass('tg-slideout-top');
            }, 500);
        },

        close: function(){
            if(this._trigger('beforeClose', null, [this._element.data('slideout-id')]) === false) {
                return;
            }
            this._element.removeClass("tg-slideout--open");
            this.showUnpinned();
            this._trigger('closed', null, [this._element.data('slideout-id')]);
        },

        toggle: function(){
            if(this._element.hasClass('tg-slideout--open')) {
                this.close(null);
            }
            else {        
                this.open();
            }
        },

        showPinned: function(){
            if(this.options.allowPin) {
                $('.tg-slideout__pin', this._element)
                    .attr('title', this.options.pinBtnTooltip)
                    .removeClass(this.options.unpinBtnClass)
                    .addClass(this.options.pinBtnClass);
            }
        },

        showUnpinned: function() {
            if(this.options.allowPin) {
                $('.tg-slideout__pin', this._element)
                    .attr('title', this.options.unpinBtnTooltip)
                    .removeClass(this.options.pinBtnClass)
                    .addClass(this.options.unpinBtnClass);
            }
        },

        bringToTop: function(){
            this._element.addClass('tg-slideout-top');
        },

        resetTop: function(){
            this._element.removeClass('tg-slideout-top');  
        },

        destroy : function() {
            var self = this;
            // cleanup code here ...
            // remove the all of the tg-slideout classes from the element
            $('.tg-slideout__close', this._element).removeClass(this.options.closeBtnClass);
            this._element[0].className = this._element[0].className.replace(/\btg-slideout*?\b/g, '');

            $('.tg-slideout__pin', this._element).removeClass('pinBtnClass').removeClass('unpinBtnClass');
            
            $.Widget.prototype.destroy.apply(self, arguments);
        },

        isParentPinned: function(){
            var isParentPinned = false;
            if(this._element.parent().hasClass('tg-slideoutContainer')) {
                isParentPinned = this._element.parent().tgSlideOutContainer('isPinned');
            }
            return isParentPinned;
        }
    });
})(jQuery);

(function($) {
    if (typeof $.tg !== 'undefined' && typeof $.tg.tgSlideOutContainer !== 'undefined') {
        return; //already defined so get out
    }

    $.widget("tg.tgSlideOutContainer", {
        options: {
            position: "left" // this can be "left" or "right"
        },

        _element: undefined,
        _currentPanel: null,
        _lastPanel: null,
        _shield: null,
        _isPinned: false,
        _panels: null,

        _create : function() {
            var self = this;
            this._element = self._element = $(self.element); 
            // add the slide out and position class to the panel
            this._element.addClass("tg-slideoutContainer");
            this._element.append("<div class='tg-slideoutContainer__shim'></div>");

            // get all of the slide out panels that are children of this container
            this._panels = $("[data-slideout-id]", this._element).tgSlideOut({
                position: this.options.position,
                allowPin: true,
                beforeOpen: function(event, slideoutID) {
                    self._trigger('beforeOpen', event, [slideoutID]);
                },
                opened: function(event, slideoutID) {
                    self._trigger('opened', event, [slideoutID]);
                },
                beforeClose: function(event, slideoutID) {
                    self._trigger('beforeClose', event, [slideoutID]);
                },
                closed: function(event, slideoutID) {
                    self._trigger('closed', event, [slideoutID]);
                },
                closeContainer: function(/*event, slideoutID*/) {
                    self.close(null);
                },
                togglePin: function(/*event, slideoutID*/) {
                    self.togglePin();
                }
            });
            this._shield = $("<div class='tg-slideoutContainer__shield'></div>")
                .on('click', function(/*event*/){
                    self.close(null);
                });
            $("body", document).append(this._shield);
        },

        _init: function () {
        },

        _setOption: function (/*key , value*/) {
            $.Widget.prototype._setOption.apply(this, arguments);
        },

        open: function(slideoutID){
            if(this._trigger('beforeOpen', null, [slideoutID]) === false) {
                return;
            }
            var self = this;
            this._lastPanel = this._currentPanel;
            this._currentPanel = slideoutID;
            $('[data-slideout-id=' + slideoutID + ']', this._element).tgSlideOut('bringToTop');
            $('[data-slideout-id=' + slideoutID + ']', this._element).tgSlideOut('open');
            if(this._lastPanel !== null) {
                setTimeout(function(){
                    self.close(self._lastPanel);
                }, 200);
            }
            if(!this._isPinned){
                this._shield.addClass('tg-slideoutContainer__shield--on');
            }
        },

        close: function(slideoutID){
            if(slideoutID === null) {
                slideoutID = this._currentPanel;
            }
            if(slideoutID !== null) {
                if(this._trigger('beforeClose', null, [slideoutID]) === false) {
                    return;
                }
                var self = this;
                this._lastPanel = null;
                $('[data-slideout-id=' + slideoutID + ']', this._element).tgSlideOut('close');
                if(this._currentPanel === slideoutID) {
                    this._shield.removeClass('tg-slideoutContainer__shield--on');
                    this._currentPanel = null;
                    if(this._isPinned){
                        this._unpin();
                    }
                }
                else {
                    setTimeout(function(){
                        $('[data-slideout-id=' + self._currentPanel + ']', this._element).tgSlideOut('resetTop');
                    }, 300);
                }
            }
        },

        toggle: function(slideoutID){
            if(this._currentPanel === slideoutID) {
                this.close(slideoutID);
            }
            else {
                this.open(slideoutID);
            }
        },

        pin: function(){
            this._panels.tgSlideOut('showPinned');
            this._element.addClass('tg-slideoutContainer--pinned');
            this._shield.removeClass('tg-slideoutContainer__shield--on');
            this._isPinned = true;
        },

        unpin: function(){
            this._unpin();
            this.close(this._currentPanel);
        },

        _unpin: function(){
            this._panels.tgSlideOut('showUnpinned');
            this._element.removeClass('tg-slideoutContainer--pinned');
            this._isPinned = false;
        },

        togglePin: function(){
            if(this._isPinned){
                this.unpin();
            }
            else {
                this.pin();
            }
        },

        isPinned: function(){
            return this._isPinned;
        },

        destroy : function() {
            var self = this;
            // cleanup code here ...

            // remove the shim
            this._element.remove(".tg-slideoutContainer__shim");
            this._shield.remove();
            this._shield = null;

            // clean up the panels
            this._panels.tgSlideOut('destroy');
            this._panels = null;

            // remove the all of the tg-slideoutController classes from the element
            this._element[0].className = this._element[0].className.replace(/\btg-slideoutController*?\b/g, '');
            this._element = undefined;

            $.Widget.prototype.destroy.apply(self, arguments);
        }
    });
})(jQuery);
