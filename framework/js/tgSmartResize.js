/*! tgSmartResize - iframe resizer - v0.1.0 - 2013-10-11
* https://github.com/tgui/tgSmartResize
* Copyright (c) 2013 Tangoe */
// See articles: http://paulirish.com/2009/throttled-smartresize-jquery-event-handler/
//               http://unscriptable.com/2009/03/20/debouncing-javascript-methods/

(function($,sr){
 
	// debouncing function from John Hann
	// http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
	var debounce = function (func, threshold, execAsap) {
		var timeout;
 
		return function debounced () {
			var obj = this, args = arguments;
			function delayed () {
				if (!execAsap) {
					func.apply(obj, args);
				}
				timeout = null; 
			}
 
			if (timeout) {
				clearTimeout(timeout);
			}
			else if (execAsap) {
				func.apply(obj, args);
			}
			timeout = setTimeout(delayed, threshold || 100); 
		};
	};

	// smartresize 
	jQuery.fn[sr] = function(fn, options){  
		var threshold;
		var execAsap;
		if(options !== undefined) {
			threshold = options.threshold;
			execAsap = options.execAsap;
		}
		return fn ? this.bind('resize orientationChanged', debounce(fn, threshold, execAsap)) : this.trigger(sr); 
	};
})(jQuery,'smartresize');
 
// usage:
//$(window).smartresize(function(){  
//  // code that takes it easy...
//});