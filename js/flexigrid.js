/*! tgDataTable - v0.1.0 - 2013-12-12
* https://github.com/tgUI/tgDataTable
* Copyright (c) 2013 Tangoe */
(function ($) {
	/*
	 * jQuery 1.9 support. browser object has been removed in 1.9 
	 */
	var browser = $.browser;
	
	if (!browser) {

		var uaMatch = function ( ua ) {
			ua = ua.toLowerCase();

			var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
				/(webkit)[ \/]([\w.]+)/.exec( ua ) ||
				/(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
				/(msie) ([\w.]+)/.exec( ua ) ||
				ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
				[];

			return {
				browser: match[ 1 ] || "",
				version: match[ 2 ] || "0"
			};
		};

		var matched = uaMatch( navigator.userAgent );
		browser = {};

		if ( matched.browser ) {
			browser[ matched.browser ] = true;
			browser.version = matched.version;
		}

		// Chrome is Webkit, but Webkit is also Safari.
		if ( browser.chrome ) {
			browser.webkit = true;
		} else if ( browser.webkit ) {
			browser.safari = true;
		}
	}
	
    /*!
     * START code from jQuery UI
     *
     * Copyright 2011, AUTHORS.txt (http://jqueryui.com/about)
     * Dual licensed under the MIT or GPL Version 2 licenses.
     * http://jquery.org/license
     *
     * http://docs.jquery.com/UI
     */
     
    if(typeof $.support.selectstart !== 'function') {
        $.support.selectstart = "onselectstart" in document.createElement("div");
    }
    
    if(typeof $.fn.disableSelection !== 'function') {
        $.fn.disableSelection = function() {
            return this.bind( ( $.support.selectstart ? "selectstart" : "mousedown" ) +
                ".ui-disableSelection", function( event ) {
                event.preventDefault();
            });
        };
    }
    
    /* END code from jQuery UI */
    
	$.addTgDataTable = function (t, p) {
		if (t.grid) {
			return false; //return if already exist
		}
		p = $.extend({ //apply default properties
			columnSelectionContainer: null,
			height: 200, //default height
			width: 'auto', //auto width
			striped: true, //apply odd even stripes
			allowSelection: 'multiple', // 'none', 'single', 'multiple', 
			defaultwidth: 100, // the default column width if a width is not specified
			minwidth: 30, //min width of columns
			maxwidth: 9000, // max width of columns
			minheight: 80, //min height of columns
			resizable: true, //allow table resizing
			bestFitColumns: true, // true to resize columns so that they best fit the available table width
			url: false, //URL if using data from AJAX
			method: 'POST', //data sending method
			errormsg: 'Connection Error',
			usepager: true,
			nowrap: true,
			page: 1, //current page
			pages: 1, // total number of pages
			total: 1, //total pages
			useResultsPerPage: true, //use the results per page select box
			resultsPerPage: 15, //results per page
			resultsPerPageOptions: [10, 15, 20, 30, 50], //allowed per-page values
			title: false,
			idProperty: 'id',
			pagestat: 'Displaying {from} to {to} of {total} items',
			pagetext: 'Page',
			outof: 'of',
			findtext: 'Find',
			params: [], //allow optional parameters to be passed around
			procmsg: 'Processing, please wait ...',
			query: '',
			qtype: '',
			nomsg: 'No items',
			minColToggle: 1, //minimum allowed column to be hidden
			showToggleBtn: true, //show or hide column toggle popup
			hideOnSubmit: true,
			autoload: true,
			blockOpacity: 0.5,
			preProcess: false,
			addTitleToCell: true, // add a title attr to cells with truncated contents
			dblClickResize: true, //auto resize column by double clicking
			onDragCol: false,
			onToggleCol: false,
			onChangeSort: false,
			onDoubleClick: false,
			onSuccess: false,
			onError: false,
			onSubmit: false, //using a custom populate function

            __mw: { //extendable middleware function holding object
                datacol: function(p, col, val) { //middleware for formatting data columns
                    var _col = (typeof p.datacol[col] === 'function') ? p.datacol[col](val) : val; //format column using function
                    if(typeof p.datacol['*'] === 'function') { //if wildcard function exists
                        return p.datacol['*'](_col); //run wildcard function
                    } else {
                        return _col; //return column without wildcard
                    }
                }
            },
            getGridClass: function(g) { //get the grid class, always returns g
                return g;
            },
            datacol: {}, //datacol middleware object 'colkey': function(colval) {}
            colResize: true, //from: http://stackoverflow.com/a/10615589
            colMove: true
		}, p);
		
		// initialize the column selection container if what was passed in is a jquery selector expression
		if(p != null && p.columnSelectionContainer != null) {
			p.columnSelectionContainer = $(p.columnSelectionContainer);
		}

		$(t).show() //show if hidden
			.attr({
				cellPadding: 0,
				cellSpacing: 0,
				border: 0
			}) //remove padding and spacing
			.removeAttr('width'); //remove width properties
		//create grid class
		var g = {
			hset: {},
			rePosDrag: function () {
				var columnDragLeft = 0 - this.headerContainer.scrollLeft;
				if (this.headerContainer.scrollLeft > 0) {
					columnDragLeft -= Math.floor(p.columnGripWidth / 2);	
				} 
				$(g.columnDragSizerContainer).css({
					top: g.headerContainer.offsetTop + 1
				});
				var columnDragPadding = this.columnDragPadding;
				var columnDragCounter=0;
				$('div', g.columnDragSizerContainer).hide();
				$('thead tr:first th:visible', this.headerContainer).each(function () {
					var n = $('thead tr:first th:visible', g.headerContainer).index(this);
					var columnDragPosition = parseInt($('div', this).width(), 10);
					if (columnDragLeft === 0) {
						columnDragLeft -= Math.floor(p.columnGripWidth / 2);	
					} 
					columnDragPosition = columnDragPosition + columnDragLeft + columnDragPadding;
					if (isNaN(columnDragPosition)) {
						columnDragPosition = 0;
					}
					$('div:eq(' + n + ')', g.columnDragSizerContainer).css({
						'left': (!(browser.mozilla) ? columnDragPosition - columnDragCounter : columnDragPosition) + 'px'
					}).show();
					columnDragLeft = columnDragPosition;
					columnDragCounter++;
				});
			},
			fixHeight: function () {
				var newHeight = $(g.bodyContainer).height();
				var headerHeight = $(this.headerContainer).height();
				$('div', this.columnDragSizerContainer).each(
					function () {
						$(this).height(newHeight + headerHeight);
					}
				);
				var gridHeight = g.bodyContainer.offsetTop + newHeight;
				if (p.height !== 'auto' && p.resizable) {
					gridHeight = g.verticalResizeDiv.offsetTop;
				}
				$(g.horizontalResizeDiv).css({
					height: gridHeight
				});
			},
			dragStart: function (dragtype, e, obj) { //default drag function start
                if (dragtype === 'colresize' && p.colResize === true) {//column resize
					var columnNumber = $('div', this.columnDragSizerContainer).index(obj);
					var th = $('th:visible:eq(' + columnNumber + ')', this.headerContainer);
					var columnWidth = $('div', th).width();
					$(obj).addClass('tg-datatable__colSizerContainer--dragging').siblings().hide();
					$(obj).prev().addClass('tg-datatable__colSizerContainer--dragging').show();
					this.colresize = {
						startX: e.pageX,
						left: parseInt(obj.style.left, 10),
						width: columnWidth,
						minwidth: th.data('minwidth'),
						maxwidth: th.data('maxwidth'),
						columnNumber: columnNumber
					};
					$('body').css('cursor', 'col-resize');
				} else if (dragtype === 'vresize') {//table resize
					var hgo = false;
					$('body').css('cursor', 'row-resize');
					if (obj) {
						hgo = true;
						$('body').css('cursor', 'col-resize');
					}
					this.vresize = {
						height: p.height,
						sizeY: e.pageY,
						width: p.width,
						sizeX: e.pageX,
						hgo: hgo
					};
				} else if (dragtype === 'colMove') {//column header drag
                    $(e.target).disableSelection(); //disable selecting the column header
                    if((p.colMove === true)) {
                        this.hset = $(this.headerContainer).offset();
                        this.hset.right = this.hset.left + $('table', this.headerContainer).width();
                        this.hset.bottom = this.hset.top + $('table', this.headerContainer).height();
                        this.dcol = obj;
                        this.dcoln = $('th', this.headerContainer).index(obj);
                        this.columnDragCopy = document.createElement("div");
                        this.columnDragCopy.className = "tg-datatable__columnDragCopy";
                        this.columnDragCopy.innerHTML = obj.innerHTML;
                        if (browser.msie) {
                            this.columnDragCopy.className = "tg-datatable__columnDragCopy ie";
                        }
                        $(this.columnDragCopy).css({
                            position: 'absolute',
                            'float': 'left',
                            display: 'none',
                            textAlign: obj.align
                        });
                        $('body').append(this.columnDragCopy);
                        $(this.columnDragSizerContainer).hide();
                    }
				}
				$('body').noSelect();
			},
			dragMove: function (e) {
				var diff = 0;
				if (this.colresize) {//column resize
					var n = this.colresize.columnNumber;
					diff = e.pageX - this.colresize.startX;
					var nleft = this.colresize.left + diff;
					var nw = this.colresize.width + diff;
					if (nw > this.colresize.minwidth && nw < this.colresize.maxwidth) {
						$('div:eq(' + n + ')', this.columnDragSizerContainer).css('left', nleft);
						this.colresize.newWidth = nw;
					}
				} else if (this.vresize) {//table resize
					var v = this.vresize;
					var y = e.pageY;
					diff = y - v.sizeY;
					if (!p.defwidth) {
						p.defwidth = p.width;
					}
					if (p.width !== 'auto' && !p.nohresize && v.hgo) {
						var x = e.pageX;
						var xdiff = x - v.sizeX;
						var newW = v.width + xdiff;
						if (newW > p.defwidth) {
							this.mainContainer.style.width = newW + 'px';
							p.width = newW;
						}
					}
					var newH = v.height + diff;
					if ((newH > p.minheight || p.height < p.minheight) && !v.hgo) {
						this.bodyContainer.style.height = newH + 'px';
						p.height = newH;
						this.fixHeight();
					}
					v = null;
				} else if (this.columnDragCopy) {
					$(this.dcol).addClass('tg-datatable__header--move').removeClass('tg-datatable__header--over');
					if (e.pageX > this.hset.right || e.pageX < this.hset.left || e.pageY > this.hset.bottom || e.pageY < this.hset.top) {
						$('body').css('cursor', 'move');
					} else {
						$('body').css('cursor', 'pointer');
					}
					$(this.columnDragCopy).css({
						top: e.pageY + 10,
						left: e.pageX + 20,
						display: 'block'
					});
				}
			},
			dragEnd: function () {
				if (this.colresize) {
					var n = this.colresize.columnNumber;
					var nw = this.colresize.newWidth;
					g.addTitleToCell($('th:visible div:eq(' + n + ')', this.headerContainer).css('width', nw));
					$('tr', this.bodyContainer).each(
						function () {
							var $tdDiv = $('td:visible div:eq(' + n + ')', this);
							$tdDiv.css('width', nw);
							g.addTitleToCell($tdDiv);
						}
					);
					this.headerContainer.scrollLeft = this.bodyContainer.scrollLeft;
					$('div:eq(' + n + ')', this.columnDragSizerContainer).siblings().show();
					$('.tg-datatable__colSizerContainer--dragging', this.columnDragSizerContainer).removeClass('tg-datatable__colSizerContainer--dragging');
					this.rePosDrag();
					this.fixHeight();
					this.colresize = false;

					// HERE WE CAN FIRE AN EVENT TO ALLOW THE APP TO SAVE THE CURRENT COLUMN SIZES
					/*
					if ($.cookies) {
						var name = p.colModel[n].name;		// Store the widths in the cookies
						$.cookie('tgDataTableWidths/'+name, nw);
					}
					*/
				} else if (this.vresize) {
					this.vresize = false;
				} else if (this.columnDragCopy) {
					$(this.columnDragCopy).remove();
					if (this.dcolt !== null) {
						if (this.dcoln > this.dcolt) {
							$('th:eq(' + this.dcolt + ')', this.headerContainer).before(this.dcol);
						}
						else {
							$('th:eq(' + this.dcolt + ')', this.headerContainer).after(this.dcol);
						}
						this.switchCol(this.dcoln, this.dcolt);
						$(this.columnDropLeftIndicator).remove();
						$(this.columnDropRightIndicator).remove();
						this.rePosDrag();
						if (p.onDragCol) {
							p.onDragCol(this.dcoln, this.dcolt);
						}
					}
					this.dcol = null;
					this.hset = null;
					this.dcoln = null;
					this.dcolt = null;
					this.columnDragCopy = null;
					$('.tg-datatable__header--move', this.headerContainer).removeClass('tg-datatable__header--move');
					$(this.columnDragSizerContainer).show();
				}
				$('body').css('cursor', 'default');
				$('body').noSelect(false);
			},
			toggleCol: function (cid, visible) {
				setTimeout(function(){
					var ncol = $("th[axis='col" + cid + "']", this.headerContainer)[0];
					var n = $('thead th', g.headerContainer).index(ncol);
					var cb = $('input[value=' + cid + ']', g.columnSelectionContainer)[0];
					if (visible == null) {
						visible = ncol.hidden;
					}
					if ($('input:checked', g.columnSelectionContainer).length < p.minColToggle && !visible) {
						return false;
					}
					if (visible) {
						ncol.hidden = false;
						$(ncol).show();
						cb.checked = true;
					} else {
						ncol.hidden = true;
						$(ncol).hide();
						cb.checked = false;
					}
					$('tbody tr', t).each(
						function () {
							if (visible) {
								$('td:eq(' + n + ')', this).show();
							} else {
								$('td:eq(' + n + ')', this).hide();
							}
						}
					);
					g.initializeColumnWidthForBestFit();
					g.rePosDrag();
					if (p.onToggleCol) {
						p.onToggleCol(cid, visible);
					}
				}, 0);
				return visible;
			},
			switchCol: function (cdrag, cdrop) { //switch columns
				$('tbody tr', t).each(
					function () {
						if (cdrag > cdrop) {
							$('td:eq(' + cdrop + ')', this).before($('td:eq(' + cdrag + ')', this));
						}
						else {
							$('td:eq(' + cdrop + ')', this).after($('td:eq(' + cdrag + ')', this));
						}
					}
				);
				//switch order in columnSelectionContainer tg-datatable__columnSelection__column
				if (cdrag > cdrop) {
					$('.tg-datatable__columnSelection__column:eq(' + cdrop + ')', this.columnSelectionContainer).before($('.tg-datatable__columnSelection__column:eq(' + cdrag + ')', this.columnSelectionContainer));
				} else {
					$('.tg-datatable__columnSelection__column:eq(' + cdrop + ')', this.columnSelectionContainer).after($('.tg-datatable__columnSelection__column:eq(' + cdrag + ')', this.columnSelectionContainer));
				}
				this.headerContainer.scrollLeft = this.bodyContainer.scrollLeft;
			},
			scroll: function () {
				this.headerContainer.scrollLeft = this.bodyContainer.scrollLeft;
				this.rePosDrag();
			},
			addData: function (data) { //parse data
				data = $.extend({rows: [], page: 0, total: 0}, data);
				if (p.preProcess) {
					data = p.preProcess(data);
				}
				$('.pReload', this.pagerContainer).removeClass('loading');
				this.loading = false;
				if (!data) {
					$('.tg-datatable-pager__stats', this.pagerContainer).html(p.errormsg);
                    if (p.onSuccess) {
						p.onSuccess(this);
                    }
					return false;
				}
				p.total = data.total;
				if (p.total === 0) {
					$('tr, a, td, div', t).unbind();
					$(t).empty();
					p.pages = 1;
					p.page = 1;
					this.buildpager();
					$('.tg-datatable-pager__stats', this.pagerContainer).html(p.nomsg);
                    if (p.onSuccess) {
						p.onSuccess(this);
                    }
					return false;
				}
				p.pages = Math.ceil(p.total / p.resultsPerPage);
				p.page = data.page;
				this.buildpager();
				//build new body
				var tbody = document.createElement('tbody');
				$.each(data.rows, function (i, row) {
					var tr = document.createElement('tr');
					var jtr = $(tr);
					if (row.name) {
						tr.name = row.name;
					}
					if (row.color) {
						jtr.css('background',row.color);
					} 
					else {
						if (i % 2 && p.striped) {
							tr.className = 'tg-datatable__row--even';
						}
					}
					if (row[p.idProperty]) {
						tr.id = 'row' + row[p.idProperty];
						jtr.attr('data-id', row[p.idProperty]);
					}
					$('thead tr:first th', g.headerContainer).each( //add cell
						function () {
							var td = document.createElement('td');
							var idx = $(this).attr('axis').substr(3);
							td.align = this.align;
							// If each row is the object itself (no 'cell' key)
							if (typeof row.cell === 'undefined') {
								td.innerHTML = row[p.colModel[idx].name];
							} else {
								// If the json elements aren't named (which is typical), use numeric order
                                var iHTML = '';
                                if (typeof row.cell[idx] !== "undefined") {
                                    iHTML = (row.cell[idx] !== null) ? row.cell[idx] : ''; //null-check for Opera-browser
                                } else {
                                    iHTML = row.cell[p.colModel[idx].name];
                                }
                                td.innerHTML = p.__mw.datacol(p, $(this).attr('abbr'), iHTML); //use middleware datacol to format cols
							}

							$(td).attr('abbr', $(this).attr('abbr'));
							$(tr).append(td);
							td = null;
						}
					);
					if ($('thead', this.mainContainer).length < 1) {//handle if grid has no headers
						for (var idx = 0; idx < row.cell.length; idx++) {
							var td = document.createElement('td');
							// If the json elements aren't named (which is typical), use numeric order
							if (typeof row.cell[idx] !== "undefined") {
								td.innerHTML = (row.cell[idx] != null) ? row.cell[idx] : '';//null-check for Opera-browser
							} else {
								td.innerHTML = row.cell[p.colModel[idx].name];
							}
							$(tr).append(td);
							td = null;
						}
					}
					$(tbody).append(tr);
					tr = null;
				});
				$('tr', t).unbind();
				$(t).empty();
				$(t).append(tbody);
				this.addCellProp();
				this.addRowProp();
				this.rePosDrag();
				tbody = null;
				data = null;
				i = null;
				if (p.onSuccess) {
					p.onSuccess(this);
				}
				if (p.hideOnSubmit) {
					$(g.blockerDiv).remove();
				}
				this.headerContainer.scrollLeft = this.bodyContainer.scrollLeft;
			},
			changeSort: function (th) { //change sortorder
				if (this.loading) {
					return true;
				}
				if (p.sortname === $(th).attr('abbr')) {
					if (p.sortorder === 'asc') {
						p.sortorder = 'desc';
					} 
					else {
						p.sortorder = 'asc';
					}
				}
				$(th).addClass('tg-datatable__header--sorted').siblings().removeClass('tg-datatable__header--sorted');
				$('.tg-datatable__header--sorted--desc', this.headerContainer).removeClass('tg-datatable__header--sorted--desc');
				$('.tg-datatable__header--sorted--asc', this.headerContainer).removeClass('tg-datatable__header--sorted--asc');
				$('div', th).addClass('tg-datatable__header--sorted--' + p.sortorder);
				p.sortname = $(th).attr('abbr');
				if (p.onChangeSort) {
					p.onChangeSort(p.sortname, p.sortorder);
				} else {
					this.populate();
				}
			},
			buildpager: function () { //rebuild pager based on new properties
				$('.tg-datatable-pager__pageNumber', this.pagerContainer).val(p.page);
				$('.tg-datatable-pager__totalPages', this.pagerContainer).html(p.pages);
				var r1 = (p.page - 1) * p.resultsPerPage + 1;
				var r2 = r1 + p.resultsPerPage - 1;
				if (p.total < r2) {
					r2 = p.total;
				}
				var stat = p.pagestat;
				stat = stat.replace(/{from}/, r1);
				stat = stat.replace(/{to}/, r2);
				stat = stat.replace(/{total}/, p.total);
				$('.tg-datatable-pager__stats', this.pagerContainer).html(stat);
			},
			populate: function () { //get latest data
				if (this.loading) {
					return true;
				}
				if (p.onSubmit) {
					var gh = p.onSubmit();
					if (!gh) {
						return false;
					}
				}
				this.loading = true;
				if (!p.url) {
					return false;
				}
				$('.tg-datatable-pager__stats', this.pagerContainer).html(p.procmsg);
				$('.pReload', this.pagerContainer).addClass('loading');
				$(g.blockerDiv).css({
					top: g.bodyContainer.offsetTop
				});
				if (p.hideOnSubmit) {
					$(this.mainContainer).prepend(g.blockerDiv);
				}
				if (!p.newp) {
					p.newp = 1;
				}
				if (p.page > p.pages) {
					p.page = p.pages;
				}
				var param = [{
					name: 'page',
					value: p.newp
				}, {
					name: 'resultsPerPage',
					value: p.resultsPerPage
				}, {
					name: 'sortname',
					value: p.sortname
				}, {
					name: 'sortorder',
					value: p.sortorder
				}, {
					name: 'query',
					value: p.query
				}, {
					name: 'qtype',
					value: p.qtype
				}];
				if (p.params.length) {
					for (var pi = 0; pi < p.params.length; pi++) {
						param[param.length] = p.params[pi];
					}
				}
				$.ajax({
					type: p.method,
					url: p.url,
					data: param,
					dataType: 'json',
					success: function (data) {
						g.addData(data);
					},
					error: function (XMLHttpRequest, textStatus, errorThrown) {
						try {
							if (p.onError) {
								p.onError(XMLHttpRequest, textStatus, errorThrown);
							}
						} catch (e) {}
					}
				});
			},
			changePage: function (ctype) { //change page
				if (this.loading) {
					return true;
				}
				p.newp = p.page; // init the new page to the current page
				switch (ctype) {
					case 'first':
						p.newp = 1;
						break;
					case 'prev':
						if (p.page > 1) {
							p.newp = parseInt(p.page, 10) - 1;
						}
						break;
					case 'next':
						if (p.page < p.pages) {
							p.newp = parseInt(p.page, 10) + 1;
						}
						break;
					case 'last':
						p.newp = p.pages;
						break;
					case 'input':
						var nv = parseInt($('.tg-datatable-pager__pageNumber', this.pagerContainer).val(), 10);
						if (isNaN(nv)) {
							nv = 1;
						}
						if (nv < 1) {
							nv = 1;
						} else if (nv > p.pages) {
							nv = p.pages;
						}
						$('.tg-datatable-pager__pageNumber', this.pagerContainer).val(nv);
						p.newp = nv;
						break;
				}
				if (p.newp === p.page) {
					return false;
				}
				if (p.onChangePage) {
					p.onChangePage(p.newp);
				} else {
					this.populate();
				}
			},
			getScrollBarWidth: function(){
				if(p.scrollBarWidth == null) {
					var inner = document.createElement('p');
					inner.style.width = "100%";
					inner.style.height = "200px";

					var outer = document.createElement('div');
					outer.style.position = "absolute";
					outer.style.top = "0px";
					outer.style.left = "0px";
					outer.style.visibility = "hidden";
					outer.style.width = "200px";
					outer.style.height = "150px";
					outer.style.overflow = "hidden";
					outer.appendChild (inner);

					document.body.appendChild (outer);
					var w1 = inner.offsetWidth;
					outer.style.overflow = 'scroll';
					var w2 = inner.offsetWidth;
					if (w1 === w2) { 
						w2 = outer.clientWidth;
					}

					document.body.removeChild (outer);

					p.scrollBarWidth = (w1 - w2);
				}
				return p.scrollBarWidth;
			},
			hasVerticalScrollBar: function(){
				return g.bodyContainer.scrollHeight > g.bodyContainer.clientHeight;
			},
			initializeColumnWidthForBestFit: function() {
				if(p.bestFitColumns) {
					var columns = []; // settings for each column
					$('thead tr:first th:visible', g.headerContainer).each(function (index) {
						columns.push({
							index: index,
							newWidth: $(this).data('initialwidth'),
							minwidth: $(this).data('minwidth'),
							maxwidth: $(this).data('maxwidth')
						});
					});

					var headerWidth = $(g.headerContainer).width() - columns.length - 1;
					if(g.hasVerticalScrollBar()){
						headerWidth = headerWidth - g.getScrollBarWidth();
					}
					var tableWidth = 0;
					$(columns).each(function () {
						tableWidth = tableWidth + this.newWidth;
					});
					var diff = headerWidth - tableWidth;
					var lastDiff = 0;
					var increment = diff > 0 ? 1 : -1;
					while(diff !== 0 &&  diff !== lastDiff) {
						lastDiff = diff;
/*jshint -W083*/
						$(columns).each( function () {
							if (diff !== 0) {
								var newWidth = this.newWidth + increment;
								if(newWidth > this.minwidth && newWidth < this.maxwidth) {
									this.newWidth = newWidth;
									diff = diff - increment;
								}
							}
						});
/*jshint +W083*/
					}
					$('thead tr:first th:visible', g.headerContainer).each(function (index) {
						var padding = $('div', this).outerWidth() - $('div', this).width();
						$('div', this).width(columns[index].newWidth - padding);
					});
					g.addCellProp(true);
				}
			},
			addCellProp: function (update) {
				if(update == null){
					update = false;
				}
				$('tbody tr td', g.bodyContainer).each(function () {
					var tdDiv = null;
					if(update){
						tdDiv = $('div:first', this);
					}
					else {
						tdDiv = document.createElement('div');
					}
					var n = $('td', $(this).parent()).index(this);
					var pth = $('th:eq(' + n + ')', g.headerContainer).get(0);
					if (pth != null) {
						$(tdDiv).css({
							textAlign: pth.align,
							width: $('div:first', pth)[0].style.width
						});
						if (pth.hidden) {
							$(this).css('display', 'none');
						}
					}
					if (p.nowrap === false) {
						$(tdDiv).css('white-space', 'normal');
					}
					if(update === false){
						if (this.innerHTML === '') {
							this.innerHTML = '&nbsp;';
						}
						tdDiv.innerHTML = this.innerHTML;
						var prnt = $(this).parent()[0];
						var pid = false;
						if (prnt.id) {
							pid = prnt.id.substr(3);
						}
						if (pth != null) {
							if (pth.process) {
								pth.process(tdDiv, pid);
							}
						}
						$(this).empty().append(tdDiv).removeAttr('width'); //wrap content
					}
					g.addTitleToCell(tdDiv);
				});
			},
			getCellDim: function (obj) {// get cell prop for editable event
				var ht = parseInt($(obj).height(), 10);
				var pht = parseInt($(obj).parent().height(), 10);
				var wt = parseInt(obj.style.width, 10);
				var pwt = parseInt($(obj).parent().width(), 10);
				var top = obj.offsetParent.offsetTop;
				var left = obj.offsetParent.offsetLeft;
				var pdl = parseInt($(obj).css('paddingLeft'), 10);
				var pdt = parseInt($(obj).css('paddingTop'), 10);
				return {
					ht: ht,
					wt: wt,
					top: top,
					left: left,
					pdl: pdl,
					pdt: pdt,
					pht: pht,
					pwt: pwt
				};
			},
			addRowProp: function () {
				var rows = $('tbody tr', g.bodyContainer);
				if(p.allowSelection === 'single' || p.allowSelection === 'multiple'){
					rows.on('click', function (e) {
						var obj = (e.target || e.srcElement);
						if (obj.href || obj.type) {
							return true;
						}
						if (e.ctrlKey || e.metaKey) {
							// mousedown already took care of this case
							return;
						}
						$(this).toggleClass('trSelected');
						if (p.allowSelection === 'single' /*&& ! g.multisel*/) {
							$(this).siblings().removeClass('trSelected');
						}
					}).on('mousedown', function (e) {
						if (p.allowSelection === 'multiple') {
							if (e.shiftKey) {
								$(this).toggleClass('trSelected');
								g.multisel = true;
								this.focus();
								$(g.mainContainer).noSelect();
							}
							if (e.ctrlKey || e.metaKey) {
								$(this).toggleClass('trSelected');
								g.multisel = true;
								this.focus();
							}
						}
					}).on('mouseup', function (e) {
						if (p.allowSelection === 'multiple') {
							if (g.multisel && ! (e.ctrlKey || e.metaKey)) {
								g.multisel = false;
								$(g.mainContainer).noSelect(false);
							}
						}
					}).hover(function (e) {
						if (p.allowSelection === 'multiple') {
							if (g.multisel && e.shiftKey) {
								$(this).toggleClass('trSelected');
							}
						}
					}, function () {});
				}
				rows.on('dblclick', function () {
					if (p.onDoubleClick) {
						p.onDoubleClick(this, g, p);
					}
				});
			},
			//Add title attribute to div if cell contents is truncated
			addTitleToCell: function(tdDiv) {
				if(p.addTitleToCell) {
					var $span = $('<span />').css('display', 'none'),
						$div = (tdDiv instanceof jQuery) ? tdDiv : $(tdDiv),
						div_w = $div.outerWidth(),
						span_w = 0;

					$('body').children(':first').before($span);
					$span.html($div.html());
					$span.css('font-size', '' + $div.css('font-size'));
					$span.css('padding-left', '' + $div.css('padding-left'));
					span_w = $span.innerWidth();
					$span.remove();

					if(span_w > div_w) {
						$div.attr('title', $div.text());
					} else {
						$div.removeAttr('title');
					}
				}
			},
			autoResizeColumn: function (obj) {
				if(!p.dblClickResize) {
					return;
				}
				var n = $('div', this.columnDragSizerContainer).index(obj),
					$th = $('th:visible:eq(' + n + ')', this.headerContainer),
					$thdiv = $('div', $th),
					ol = parseInt(obj.style.left, 10),
					ow = $thdiv.width(),
					nw = 0,
					nl = 0,
					$span = $('<span />');
				var minwidth = $th.data('minwidth');
				var maxwidth = $th.data('maxwidth');

				$('body').children(':first').before($span);
				$span.html($thdiv.html());
				$span.css('font-size', '' + $thdiv.css('font-size'));
				$span.css('padding-left', '' + $thdiv.css('padding-left'));
				$span.css('padding-right', '' + $thdiv.css('padding-right'));
				nw = $span.width();
				$('tr', this.bodyContainer).each(function () {
					var $tdDiv = $('td:visible div:eq(' + n + ')', this),
						spanW = 0;
					$span.html($tdDiv.html());
					$span.css('font-size', '' + $tdDiv.css('font-size'));
					$span.css('padding-left', '' + $tdDiv.css('padding-left'));
					$span.css('padding-right', '' + $tdDiv.css('padding-right'));
					spanW = $span.width();
					nw = (spanW > nw) ? spanW : nw;
				});
				$span.remove();
				nw = (minwidth > nw) ? minwidth : nw;
				nw = (maxwidth < nw) ? maxwidth : nw;
				nl = ol + (nw - ow);
				$('div:eq(' + n + ')', this.columnDragSizerContainer).css('left', nl);
				this.colresize = {
					newWidth: nw,
					columnNumber: n
				};
				g.dragEnd();
			},
			pager: 0
		};
        
        g = p.getGridClass(g); //get the grid class
        var thead = null;
		if (p.colModel) { //create model if any
			thead = document.createElement('thead');
			var tr = document.createElement('tr');
			for (var i = 0; i < p.colModel.length; i++) {
				var cm = p.colModel[i];
				var th = document.createElement('th');
				$(th).attr('axis', 'col' + i);
				if( cm ) {	// only use cm if its defined
					// ~~~ HERE WE CAN GET THE SAVED COLUMN POSITIONS AND RESET THEM FROM THE SAVED WIDTHS
					/*
					if ($.cookies) {
						var cookie_width = 'tgDataTableWidths/'+cm.name;		// Re-Store the widths in the cookies
						if( $.cookie(cookie_width) != undefined ) {
							cm.width = $.cookie(cookie_width);
						}
					}
					*/
					if( cm.display !== undefined ) {
						th.innerHTML = cm.display;
					}
					if (cm.name && cm.sortable) {
						$(th).attr('abbr', cm.name);
					}
					if (cm.align) {
						th.align = cm.align;
					}
					if (cm.width) {
						$(th).attr('width', cm.width);
					}
					if (cm.minwidth) {
						$(th).data('minwidth', cm.minwidth);
					}
					if (cm.maxwidth) {
						$(th).data('maxwidth', cm.maxwidth);
					}
					if ($(cm).attr('hide')) {
						th.hidden = true;
					}
					if (cm.process) {
						th.process = cm.process;
					}
				} else {
					th.innerHTML = "";
					$(th).attr('width', p.defaultwidth);
				}
				$(tr).append(th);
			}
			$(thead).append(tr);
			$(t).prepend(thead);
		} // end if p.colmodel
		//init divs
		g.mainContainer = document.createElement('div'); //create global container
		g.headerContainer = document.createElement('div'); //create header container
		g.bodyContainer = document.createElement('div'); //create body container
		g.verticalResizeDiv = document.createElement('div'); //create grip
		g.horizontalResizeDiv = document.createElement('div'); //create horizontal resizer
		g.columnDragSizerContainer = document.createElement('div'); //create column drag
		g.blockerDiv = document.createElement('div'); //create blocker
		g.columnSelectionContainer = p.columnSelectionContainer;
		g.pagerContainer = document.createElement('div'); //create pager container
        
        if(p.colResize === false) { //don't display column drag if we are not using it
            $(g.columnDragSizerContainer).css('display', 'none');
        }
        
		if (!p.usepager) {
			g.pagerContainer.style.display = 'none';
		}
		g.hTable = document.createElement('table');
		g.mainContainer.className = 'tg-datatable';
		if (p.width !== 'auto') {
			g.mainContainer.style.width = p.width + (isNaN(p.width) ? '' : 'px');
		} 
		//add conditional classes
		if (browser.msie) {
			$(g.mainContainer).addClass('ie');
		}
		$(t).before(g.mainContainer);
		$(g.mainContainer).append(t);
		g.headerContainer.className = 'tg-datatable__header';

		$(t).before(g.headerContainer);
		g.hTable.cellPadding = 0;
		g.hTable.cellSpacing = 0;
		$(g.headerContainer).append('<div class="tg-datatable__header__columnContainer"></div>');
		$('div', g.headerContainer).append(g.hTable);
		thead = $("thead:first", t).get(0);
		if (thead) {
			$(g.hTable).append(thead);
		}
		thead = null;
		if (!p.colmodel) {
			var ci = 0;
		}
		$('thead tr:first th', g.headerContainer).each(function () {
			var thdiv = document.createElement('div');
			if ($(this).attr('abbr')) {
				$(this).click(function (e) {
					if (!$(this).hasClass('tg-datatable__header--over')) {
						return false;
					}
					var obj = (e.target || e.srcElement);
					if (obj.href || obj.type) {
						return true;
					}
					g.changeSort(this);
				});
				if ($(this).attr('abbr') === p.sortname) {
					this.className = 'tg-datatable__header--sorted';
					thdiv.className = 'tg-datatable__header--sorted--' + p.sortorder;
				}
			}
			if (this.hidden) {
				$(this).hide();
			}
			if (!p.colmodel) {
				$(this).attr('axis', 'col' + ci++);
			}
			
			// if there isn't a default width, then the column headers don't match
			// i'm sure there is a better way, but this at least stops it failing
			if (this.width === '') {
				this.width = p.defaultwidth;
			}
			if ($(this).data('minwidth') == null) {
				$(this).data('minwidth', p.minwidth);
			}
			if ($(this).data('maxwidth') == null) {
				$(this).data('maxwidth', p.maxwidth);
			}
			
			// check to see if the width is between the min and max, if its not adjust it
			if(this.width < $(this).data('minwidth')) {
				this.width = $(this).data('minwidth');
			}
			if(this.width > $(this).data('maxwidth')) {
				this.width = $(this).data('maxwidth');
			}
			$(this).data('initialwidth', (typeof this.width === 'string') ? parseInt(this.width ,10): this.width);

			$(thdiv).css({
				textAlign: this.align,
				width: this.width + 'px'
			});
			thdiv.innerHTML = this.innerHTML;
			g.addTitleToCell(thdiv); 
			$(this).empty().append(thdiv).removeAttr('width').mousedown(function (e) {
				g.dragStart('colMove', e, this);
			}).hover(function () {
				if (!g.colresize && !$(this).hasClass('tg-datatable__header--move') && !g.columnDragCopy) {
					$(this).addClass('tg-datatable__header--over');
				}
				if ($(this).attr('abbr') !== p.sortname && !g.columnDragCopy && !g.colresize && $(this).attr('abbr')) {
					$('div', this).addClass('tg-datatable__header--sorted--' + p.sortorder);
				} else if ($(this).attr('abbr') === p.sortname && !g.columnDragCopy && !g.colresize && $(this).attr('abbr')) {
					var no = (p.sortorder === 'asc') ? 'desc' : 'asc';
					$('div', this).removeClass('s' + p.sortorder).addClass('tg-datatable__header--sorted--' + no);
				}
				if (g.columnDragCopy) {
					var n = $('th', g.headerContainer).index(this);
					if (n === g.dcoln) {
						return false;
					}
					if (n < g.dcoln) {
						$(this).append(g.columnDropLeftIndicator);
					} else {
						$(this).append(g.columnDropRightIndicator);
					}
					g.dcolt = n;
				} 
			}, function () {
				$(this).removeClass('tg-datatable__header--over');
				if ($(this).attr('abbr') !== p.sortname) {
					$('div', this).removeClass('tg-datatable__header--sorted--' + p.sortorder);
				} else if ($(this).attr('abbr') === p.sortname) {
					var no = (p.sortorder === 'asc') ? 'desc' : 'asc';
					$('div', this).addClass('s' + p.sortorder).removeClass('tg-datatable__header--sorted--' + no);
				}
				if (g.columnDragCopy) {
					$(g.columnDropLeftIndicator).remove();
					$(g.columnDropRightIndicator).remove();
					g.dcolt = null;
				}
			}); //wrap content
		});

		//set bodyContainer
		g.bodyContainer.className = 'tg-datatable__body';
		$(t).before(g.bodyContainer);
		$(g.bodyContainer).css({
			height: (p.height === 'auto') ? 'auto' : p.height + "px"
		}).scroll(function (/*e*/) {
			g.scroll();
		}).append(t);
		if (p.height === 'auto') {
			$('table', g.bodyContainer).addClass('tg-datatable--autoheight');
		}

		//add td & row properties
		g.addCellProp();
		g.addRowProp();

		//try to best fit the columns 
		g.initializeColumnWidthForBestFit();

        //set columnDragSizerContainer only if we are using it
        if (p.colResize === true) {
            var cdcol = $('thead tr:first th:first', g.headerContainer).get(0);
            if(cdcol !== null) {
                g.columnDragSizerContainer.className = 'tg-datatable__colSizerContainer';
                g.columnDragPadding = 0;
                g.columnDragPadding += (isNaN(parseInt($('div', cdcol).css('borderLeftWidth'), 10)) ? 0 : parseInt($('div', cdcol).css('borderLeftWidth'), 10));
                g.columnDragPadding += (isNaN(parseInt($('div', cdcol).css('borderRightWidth'), 10)) ? 0 : parseInt($('div', cdcol).css('borderRightWidth'), 10));
                g.columnDragPadding += (isNaN(parseInt($('div', cdcol).css('paddingLeft'), 10)) ? 0 : parseInt($('div', cdcol).css('paddingLeft'), 10));
                g.columnDragPadding += (isNaN(parseInt($('div', cdcol).css('paddingRight'), 10)) ? 0 : parseInt($('div', cdcol).css('paddingRight'), 10));
                g.columnDragPadding += (isNaN(parseInt($(cdcol).css('borderLeftWidth'), 10)) ? 0 : parseInt($(cdcol).css('borderLeftWidth'), 10));
                g.columnDragPadding += (isNaN(parseInt($(cdcol).css('borderRightWidth'), 10)) ? 0 : parseInt($(cdcol).css('borderRightWidth'), 10));
                g.columnDragPadding += (isNaN(parseInt($(cdcol).css('paddingLeft'), 10)) ? 0 : parseInt($(cdcol).css('paddingLeft'), 10));
                g.columnDragPadding += (isNaN(parseInt($(cdcol).css('paddingRight'), 10)) ? 0 : parseInt($(cdcol).css('paddingRight'), 10));
                $(g.bodyContainer).before(g.columnDragSizerContainer);
                var cdheight = $(g.bodyContainer).height();
                var hdheight = $(g.headerContainer).height();
                $(g.columnDragSizerContainer).css({
                    top: -hdheight + 'px'
                });
                $('thead tr:first th', g.headerContainer).each(function() {
                    var columnGripDiv = document.createElement('div');
                    $(g.columnDragSizerContainer).append(columnGripDiv);
                    if (!p.columnGripWidth) {
                        p.columnGripWidth = $(columnGripDiv).width();
                    }
                    $(columnGripDiv).css({
                        height: cdheight + hdheight
                    }).mousedown(function(e) {
                        g.dragStart('colresize', e, this);
                    }).dblclick(function(/*e*/) {
                        g.autoResizeColumn(this);
                    });
                });
            }
        }
		//add strip
		if (p.striped) {
			$('tbody tr:odd', g.bodyContainer).addClass('tg-datatable__row--even');
		}
		if (p.resizable && p.height !== 'auto') {
			g.verticalResizeDiv.className = 'tg-datatable__resizer--vertical';
			$(g.verticalResizeDiv).mousedown(function (e) {
				g.dragStart('vresize', e);
			}).html('<span></span>');
			$(g.bodyContainer).after(g.verticalResizeDiv);
		}
		if (p.resizable && p.width !== 'auto' && !p.nohresize) {
			g.horizontalResizeDiv.className = 'tg-datatable__resizer--horizontal';
			$(g.horizontalResizeDiv).mousedown(function (e) {
				g.dragStart('vresize', e, true);
			}).html('<span></span>').css('height', $(g.mainContainer).height());
			$(g.mainContainer).append(g.horizontalResizeDiv);
		}
		// add pager
		if (p.usepager) {
			g.pagerContainer.className = 'tg-datatable__pager';
			g.pagerContainer.innerHTML = '<div class="tg-datatable__pager__controls"></div>';
			$(g.bodyContainer).after(g.pagerContainer);
			//var html = ' <div class="pGroup"> <div class="tgIMG-icon-small--first"><span></span></div><div class="tgIMG-icon-small--prev"><span></span></div> </div> <div class="btnseparator"></div> <div class="pGroup"><span class="pcontrol">' + p.pagetext + ' <input type="text" size="4" value="1" /> ' + p.outof + ' <span> 1 </span></span></div> <div class="btnseparator"></div> <div class="pGroup"> <div class="tgIMG-icon-small--next"><span></span></div><div class="tgIMG-icon-small--last"><span></span></div> </div> <div class="btnseparator"></div> <div class="pGroup"> <div class="tgIMG-icon--refresh"><span></span></div> </div> <div class="btnseparator"></div> <div class="pGroup"><span class="pPageStat"></span></div>';
			var html = ' <div class="tg-datatable-pager__button tg-datatable-pager__button--first"><div class="tgIMG-icon-small--first">&nbsp;</div></div> <div class="tg-datatable-pager__button tg-datatable-pager__button--prev"><div class="tgIMG-icon-small--prev">&nbsp;</div></div> <div><label>' + p.pagetext + ' <input class="tg-datatable-pager__pageNumber tg-input--xsmall" type="text" size="4" value="1" /> ' + p.outof + ' <span class="tg-datatable-pager__totalPages"> 1 </span></label></div> <div class="tg-datatable-pager__button tg-datatable-pager__button--next"><div class="tgIMG-icon-small--next">&nbsp;</div></div> <div class="tg-datatable-pager__button tg-datatable-pager__button--last"><div class="tgIMG-icon-small--last">&nbsp;</div></div> <div class="tg-datatable-pager__button tg-datatable-pager__button--refresh"><div class="tgIMG-icon--refresh">&nbsp;</div></div> <div><span class="tg-datatable-pager__stats"></span></div>';
			$('div', g.pagerContainer).html(html);
			$('.tg-datatable-pager__button--refresh', g.pagerContainer).click(function () {
				g.populate();
			});
			$('.tg-datatable-pager__button--first', g.pagerContainer).click(function () {
				g.changePage('first');
			});
			$('.tg-datatable-pager__button--prev', g.pagerContainer).click(function () {
				g.changePage('prev');
			});
			$('.tg-datatable-pager__button--next', g.pagerContainer).click(function () {
				g.changePage('next');
			});
			$('.tg-datatable-pager__button--last', g.pagerContainer).click(function () {
				g.changePage('last');
			});
			$('.tg-datatable-pager__pageNumber', g.pagerContainer).keydown(function (e) {
				if (e.keyCode === 13) { 
                    g.changePage('input');
				}
			});
			if (p.useResultsPerPage) {
				var opt = '',
					sel = '';
				for (var nx = 0; nx < p.resultsPerPageOptions.length; nx++) {
					if (p.resultsPerPage === p.resultsPerPageOptions[nx]) {
						sel = 'selected="selected"';
					}
					else { 
						sel = '';
					}
					opt += "<option value='" + p.resultsPerPageOptions[nx] + "' " + sel + " >" + p.resultsPerPageOptions[nx] + "&nbsp;&nbsp;</option>";
				}
				$('.tg-datatable__pager__controls', g.pagerContainer).prepend("<div><select name='resultsPerPage'>" + opt + "</select></div> ");
				$('select', g.pagerContainer).change(function () {
					if (p.onRpChange) {
						p.onRpChange(+this.value);
					} else {
						p.newp = 1;
						p.resultsPerPage = +this.value;
						g.populate();
					}
				});
			}
		}
		//setup columnDropIndicators
		g.columnDropLeftIndicator = document.createElement('span');
		g.columnDropLeftIndicator.className = 'tg-datatable__columnDrop--left';
		g.columnDropRightIndicator = document.createElement('span');
		g.columnDropRightIndicator.className = 'tg-datatable__columnDrop--right';
		//add block
		g.blockerDiv.className = 'tg-datatable__blocker';
		var gh = $(g.bodyContainer).height();
		var gtop = g.bodyContainer.offsetTop;
		$(g.blockerDiv).css({
			width: g.bodyContainer.style.width,
			height: gh,
			background: 'white',
			position: 'relative',
			marginBottom: (gh * -1),
			zIndex: 1,
			top: gtop,
			left: '0px'
		});
		$(g.blockerDiv).fadeTo(0, p.blockOpacity);
		// add column control
		if ($('th', g.headerContainer).length) {
			if(g.columnSelectionContainer != null) {
				g.columnSelectionContainer.addClass('tg-datatable__columnSelection');
				var cn = 0;
				$('th div', g.headerContainer).each(function () {
					var kcol = $("th[axis='col" + cn + "']", g.headerContainer)[0];
					var chk = 'checked="checked"';
					if (kcol.style.display === 'none') {
						chk = '';
					}
					g.columnSelectionContainer.append('<div class="tg-datatable__columnSelection__column"><label><input type="checkbox" ' + chk + ' class="togCol" value="' + cn + '" /> ' + this.innerHTML + '</label></div>');
					cn++;
				});

				$('input[type="checkbox"]', g.columnSelectionContainer).click(
					function(){
						if ($('input:checked', g.columnSelectionContainer).length < p.minColToggle && this.checked === false) {
							return false;
						}
						g.toggleCol($(this).val());
				});
			}
		}
		//add document events
		$(document).mousemove(function (e) {
			g.dragMove(e);
		}).mouseup(function (/*e*/) {
			g.dragEnd();
		}).hover(function () {}, function () {
			g.dragEnd();
		});
		g.rePosDrag();
		g.fixHeight();
		//make grid functions accessible
		t.p = p;
		t.grid = g;
		// load data
		if (p.url && p.autoload) {
			g.populate();
		}

		return t;
	};
	var docloaded = false;
	$(document).ready(function () {
		docloaded = true;
	});
	$.fn.tgDataTable = function (p) {
		return this.each(function () {
			if (!docloaded) {
				$(this).hide();
				var t = this;
				$(document).ready(function () {
					$.addTgDataTable(t, p);
				});
			} else {
				$.addTgDataTable(this, p);
			}
		});
	}; //end tgDataTable
	$.fn.tgDataTableReload = function (/*p*/) { // function to reload grid
		return this.each(function () {
			if (this.grid && this.p.url) {
				this.grid.populate();
			}
		});
	}; //end tgDataTableReload
	$.fn.tgDataTableOptions = function (p) { //function to update general options
		return this.each(function () {
			if (this.grid) {
				$.extend(this.p, p);
			}
		});
	}; //end tgDataTableOptions
	$.fn.tgDataTableToggleCol = function (cid, visible) { // function to reload grid
		return this.each(function () {
			if (this.grid) {
				this.grid.toggleCol(cid, visible);
			}
		});
	}; //end tgDataTableToggleCol
	$.fn.tgDataTableAddData = function (data) { // function to add data to grid
		return this.each(function () {
			if (this.grid) {
				this.grid.addData(data);
			}
		});
	};
	$.fn.noSelect = function (p) { //no select plugin by me :-)
		var prevent = (typeof p === 'undefined' || p === null) ? true : p;
		if (prevent) {
			return this.each(function () {
				if (browser.mozilla) {
					$(this).css('MozUserSelect', 'none');
					$('body').trigger('focus');
				}
				else {
					$(this).bind('selectstart', function () {
						return false;
					});
				}
			});
		} else {
			return this.each(function () {
				if (browser.mozilla) {
					$(this).css('MozUserSelect', 'inherit');
				}
				else {
					$(this).unbind('selectstart');
				}
			});
		}
	}; //end noSelect
	$.fn.tgDataTableSearch = function(/*p*/) { // function to search grid
		return this.each( function() { 
			if (this.grid&&this.p.searchitems) {
				this.grid.doSearch(); 
			}
		});
	}; //end tgDataTableSearch
	$.fn.selectedRows = function (/*p*/) { // Returns the selected rows as an array, taken and adapted from http://stackoverflow.com/questions/11868404/flexigrid-get-selected-row-columns-values
		var arReturn = [];
		var arRow = [];
		var selector = $(this.selector + ' .trSelected');


		$(selector).each(function (i, row) {
			arRow = [];
			var idr = $(row).data('id');
			$.each(row.cells, function (c, cell) {
				var col = cell.abbr;
				var val = cell.firstChild.innerHTML;
				if (val === '&nbsp;') {
					val = '';      // Trim the content
				}
				var idx = cell.cellIndex;

				arRow.push({
					Column: col,        // Column identifier
					Value: val,         // Column value
					CellIndex: idx,     // Cell index
					RowIdentifier: idr  // Identifier of this row element
				});
			});
			arReturn.push(arRow);
		});
		return arReturn;
	};
})(jQuery);
